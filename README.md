# [dockerfiles]


<br/>

# Ubuntu and JDK8

### Download DockerFile:

    $ curl -o ubuntu-jdk8.dockerfile https://raw.githubusercontent.com/javadev-org/docker/master/ubuntu-jdk8.dockerfile


### Build:

    $ docker build -f ubuntu-jdk8.dockerfile -t ubuntu/jdk8 .


### RUN:

    $ docker run -it --rm ubuntu/jdk8 java -version


<br/>

# Oracle Linux and JDK8

### Download:

    $ curl -o oraclelinux-jdk8.dockerfile https://raw.githubusercontent.com/javadev-org/dockerfiles/master/JDK8/oraclelinux-jdk8.dockerfile

    $ wget --no-check-certificate --no-cookies - --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u91-b14/server-jre-8u91-linux-x64.tar.gz


# Build:

    $ docker build -f oraclelinux-jdk8.dockerfile -t oraclelinux/jdk8 .


# RUN:

    $ docker run -it --rm oraclelinux/jdk8 java -version


<br/>

By Materials from:  
https://github.com/dockerfile/java  
https://github.com/oracle/docker-images
