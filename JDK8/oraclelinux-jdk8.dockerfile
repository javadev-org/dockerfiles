# Download: wget --no-check-certificate --no-cookies - --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u91-b14/server-jre-8u91-linux-x64.tar.gz
# Build: docker build -f oraclelinux-jdk8.dockerfile -t oraclelinux/jdk8 .
# RUN: docker run -it --rm oraclelinux/jdk8 java -version

FROM oraclelinux:latest

MAINTAINER marley (www.javadev.org)

RUN yum update -y
RUN yum install -y wget tar vim

ENV JAVA_PKG=server-jre-8u*-linux-x64.tar.gz \
    JAVA_HOME=/usr/java/default \
    PATH=$PATH:/usr/java/default/bin

ADD $JAVA_PKG /usr/java/

RUN mv $(ls -1 -d /usr/java/*) $JAVA_HOME
