# Build: docker build -f ubuntu-jdk8.dockerfile -t ubuntu/jdk8 .
# RUN: docker run -it --rm ubuntu/jdk8 java -version

FROM ubuntu:14.04
MAINTAINER marley (www.javadev.org)

RUN apt-get update
RUN apt-get install -y --no-install-recommends software-properties-common


# Install Java.
RUN \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer


# Define working directory.
WORKDIR /data

# Define commonly used JAVA_HOME variable
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

# Define default command.
CMD ["bash"]
